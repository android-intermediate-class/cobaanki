import 'package:flutter/material.dart';
import 'package:flutter_inappwebview_quill/flutter_inappwebview_quill.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: InAppWebView(
          // initialUrlRequest: URLRequest(
          //   url: Uri.parse(
          //       "https://enterprise.dev.pintar.co/id/course/c50c3852-3b21-41ae-b60f-51c27e58c034?quiz=f8962d01-eb0e-4361-ae68-99318b37abd7"),
          //   headers: {'Authorization': 'Bearer $token'},
          // ),
          // initialOptions: InAppWebViewGroupOptions(
          //   crossPlatform: InAppWebViewOptions(
          //     supportZoom: false,
          //     preferredContentMode: UserPreferredContentMode.MOBILE,
          //   ),
          // ),
          onWebViewCreated: (InAppWebViewController controller) {
            controller.loadUrl(
                urlRequest: URLRequest(
                    url: Uri.parse(
                        "https://enterprise.dev.pintar.co/id/course/c50c3852-3b21-41ae-b60f-51c27e58c034?quiz=f8962d01-eb0e-4361-ae68-99318b37abd7"),
                    headers: {
                  'Authorization':
                      'Bearer eyJraWQiOiJGVVJmXC9hUkIwREdvelwvdkhFNDRJdEpcL1FMYmoreUkrU0J5R3ZTMTZHeW93PSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI0ZGU3MTI0ZC1lZWI4LTRkMjYtOGMxZS1jMDk2NjEwZWM3MGIiLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuYXAtc291dGhlYXN0LTEuYW1hem9uYXdzLmNvbVwvYXAtc291dGhlYXN0LTFfU2VpOGZ2cjFMIiwiY2xpZW50X2lkIjoiM3BibThlZTc3cGVmZTd1cjRtN24xY284Mm4iLCJvcmlnaW5fanRpIjoiMTVmYzlhOTYtYzI1NC00ZjNlLWE1NTctM2FkODJiNWQ0NzBhIiwiZXZlbnRfaWQiOiI1NmMzY2QzZi1lM2E0LTRkMzQtYTRmNS1lMzMzMTIxMjdhNmUiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJzY29wZSI6ImF3cy5jb2duaXRvLnNpZ25pbi51c2VyLmFkbWluIiwiYXV0aF90aW1lIjoxNzE1OTQ4OTI1LCJleHAiOjE3MTYxOTQyODIsImlhdCI6MTcxNjE4NzA4MiwianRpIjoiMTFkODE3NzMtNDUxOS00NzQ1LWI3NDEtZGJjNWQyOGU2YTBjIiwidXNlcm5hbWUiOiI0ZGU3MTI0ZC1lZWI4LTRkMjYtOGMxZS1jMDk2NjEwZWM3MGIifQ.DcXeV6qpsl0CqkNBaCBXl9BW0urQHn3zlnjV2fiGzKAlMPRSAXsqU1NXUBa5VK8VAhOAogRVDOrpu-8pkWkm5uEE6JgFQ5MAgu54TqcgGsoRhwPEKG_Nj9c7PBtVI_dkn3PEpjZrKeNxX99aPMYnUQ149MJfhY618qbvMY8yXWiIqTmNYgSTD9DgCu3ldAy_gKGYNXtqvavckPKw7RlTTFSyCR6ADo5Xezs91VcRaramEnX2koS3zRZZWVKIn2I228i3sWtOkrBxnbxWmr9hbYRUC1wVhwJ0OWMw_MtvTM4MUqA6qGadPw-NnfPoOQaCUNl6bDGzz05nMsGIATNZvA'
                }));
          },

          androidOnPermissionRequest: (InAppWebViewController controller,
              String origin, List<String> resources) async {
            return PermissionRequestResponse(
                resources: resources,
                action: PermissionRequestResponseAction.GRANT);
          },
        ),
      ),
    );
  }
}
